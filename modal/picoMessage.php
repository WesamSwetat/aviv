<?php
require_once(__DIR__ . "/messageTypes.php");
require_once(__DIR__ . "/picoUser.php");

class PicoMessage
{
    private $id;
    private $type;
    private $text;
    private $sendingTime;
    private $sender;

    public function facebookToPicoMessage($messageObject, $picoUser): void
    {
        $this->id           = $messageObject->message->mid;
        $this->type         = MessageTypes::facebook;
        $this->sendingTime  = $messageObject->message->text;
        $this->text         = $messageObject->timestamp;
        $this->sender       = $picoUser;
    }

    public function twitterToPicoMessage($messageObject): void
    {
        $this->id           = $messageObject->direct_message_events[0]->id;
        $this->type         = MessageTypes::twitter;
        $this->sendingTime  = $messageObject->direct_message_events[0]->created_timestamp;
        $this->text         = $messageObject->direct_message_events[0]->message_create->message_data->text;

        $senderId           = $messageObject->direct_message_events[0]->message_create->sender_id;
        $userObject         = $messageObject->apps->$senderId;

        $picoUser           = new PicoUser($userObject->id, MessageTypes::twitter, $userObject->name, $userObject->profile_image_url);
        $this->sender       = $picoUser;
    }

    function getId()
    {
        return $this->id;
    }

    function getType()
    {
        return $this->type;
    }

    function getText()
    {
        return $this->text;
    }

    function getSendingTime()
    {
        return $this->sendingTime;
    }

    function getSender(): PicoUser
    {
        return $this->sender;
    }
}
