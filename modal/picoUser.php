<?php

class PicoUser {
    private $id;
    private $type;
    private $name;
    private $profilePicture;

    function __construct($id, $type, $name, $profilePicture) {
        $this->id              = $id;
        $this->type            = $type;
        $this->name            = $name;
        $this->profilePicture  = $profilePicture;
    }

    function getId() {
        return $this->id;
    }

    function getType() {
        return $this->type;
    }

    function getName() {
        return $this->name;
    }

    function getProfilePicture() {
        return $this->profilePicture;
    }
}

?>