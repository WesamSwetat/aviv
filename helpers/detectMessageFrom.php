<?php
require_once(__DIR__ . "/../modal/picoMessage.php");
require_once(__DIR__ . "/facebookUserInfo.php");
require_once(__DIR__ . "/handleMessage.php");

class DetectMessageFrome extends HandleMessage
{
    function detect($messaged)
    {

        $picoMessage = new PicoMessage();

        if (isset($messaged->sender) && isset($messaged->timestamp) && isset($messaged->message)) {
            // if true that mean this is a facebook message
            // structure from https://developers.facebook.com/docs/messenger-platform/reference/webhook-events/messages => Text Message
            $facebookUserInfo = new FacebookUserInfo();
            $picoUser = $facebookUserInfo->getFacebookUserInfo($messaged->sender);
            $picoMessage->facebookToPicoMessage($messaged, $picoUser);
            $this->executeAll($picoMessage);
            print_r($picoMessage);
            echo "$$$$$$$$$$$$$$$$$$$$$$ SUCCESS $$$$$$$$$$$$$$$$$$$$$$";
        } else if (isset($messaged->for_user_id) && isset($messaged->direct_message_events) && isset($messaged->apps)) {
            // if true that mean this is a twitter message
            // structure from https://developer.twitter.com/en/docs/accounts-and-users/subscribe-account-activity/guides/account-activity-data-objects#direct_message_events
            $picoMessage->twitterToPicoMessage($messaged);
            $this->executeAll($picoMessage);
            print_r($picoMessage);
            echo "$$$$$$$$$$$$$$$$$$$$$$ SUCCESS $$$$$$$$$$$$$$$$$$$$$$";
        } else {
            // something else
            echo "!!!!!!!!!!!!!!!!!!!!!! FAIL !!!!!!!!!!!!!!!!!!!!!!";
        }
    }
}
