<?php
require_once(__DIR__ . "/../modal/picoMessage.php");
require_once(__DIR__ . "/../utilities/cache.php");
require_once(__DIR__ . "/../utilities/database.php");
require_once(__DIR__ . "/../utilities/picoAIanalysis.php");
require_once(__DIR__ . "/../utilities/webSocket.php");

abstract class HandleMessage
{

    protected function executeAll(PicoMessage $picoMessage): void
    {
        $this->saveCache($picoMessage)->saveOnDatabase($picoMessage)->sendToPicoAlanalysis($picoMessage)->webSocketUpdate($picoMessage);
    }

    protected function saveCache(PicoMessage $picoMessage)
    {
        $cash = new Cache();
        $cash->save($picoMessage);
        return $this;
    }

    protected function saveOnDatabase(PicoMessage $picoMessage)
    {
        $database = new Database();
        $database->save($picoMessage);
        return $this;
    }

    protected function sendToPicoAlanalysis(PicoMessage $picoMessage)
    {
        $picoAlanalysis = new PicoAlanalysis();
        $picoAlanalysis->alanalysis($picoMessage);
        return $this;
    }

    protected function webSocketUpdate(PicoMessage $picoMessage)
    {
        $webSocketUpdate = new WebSocketUpdate();
        $webSocketUpdate->update($picoMessage);
        return $this;
    }
}
