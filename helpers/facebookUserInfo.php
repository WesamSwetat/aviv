<?php
require_once(__DIR__ . "/../modal/picoUser.php");
require_once(__DIR__ . "/../modal/messageTypes.php");
class FacebookUserInfo
{
    private const PAGE_ACCESS_TOKEN = "your page access token";
    private const IS_LIVE           = false;
    function getFacebookUserInfo($psid): PicoUser
    {
        // by facebook Docs https://developers.facebook.com/docs/messenger-platform/identity/user-profile => Retrieving a Person's Profile
        // get user info by API => https://graph.facebook.com/<PSID>?fields=first_name,last_name,profile_pic&access_token=<PAGE_ACCESS_TOKEN>
        // get fields => first_name, last_name, profile_pic

        // $testHttpCall = file_get_contents("https://jsonplaceholder.typicode.com/photos"); // 

        if (self::IS_LIVE) {
            $url = "https://graph.facebook.com/" . $psid . "?fields=first_name,last_name,profile_pic&access_token=" . self::PAGE_ACCESS_TOKEN;
            $response = file_get_contents($url);
            $response = json_decode($response);
            //If json_decode failed, the JSON is invalid.
            if (!is_object($response)) {
                throw new Exception('Received content contained invalid JSON!');
            }
            return new PicoUser($psid, MessageTypes::facebook,  $response->first_name . ' ' . $response->last_name, $response->profile_pic); 
        }
        // return mock data
        return new PicoUser("2054384671296585", MessageTypes::facebook,  "Wesam Swetat", "https://platform-lookaside.fbsbx.com/platform/profilepic/?psid=2054384671296585&width=1024&ext=1558376027&hash=AeQPPrLCMeFzO89D"); 
    }
}
